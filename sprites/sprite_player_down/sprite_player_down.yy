{
    "id": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27d88a8f-d73d-4efd-ae0b-73a7fb0060f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "bfe3921f-ada3-4183-b756-02829504b003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d88a8f-d73d-4efd-ae0b-73a7fb0060f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d70a1b-37a2-4df1-ac5c-a193cbf2cb4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d88a8f-d73d-4efd-ae0b-73a7fb0060f6",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "dbd219a6-845c-4065-a4df-a3c55b1f3f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "3819f253-4404-4b0b-85f9-8e9faf021023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd219a6-845c-4065-a4df-a3c55b1f3f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e2fc6e-9b4e-4fc6-b769-daa432e27984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd219a6-845c-4065-a4df-a3c55b1f3f29",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "c46be850-1263-415f-995c-b4091868bc81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "5d154304-58bd-4450-a1d0-ef8e839a7d5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46be850-1263-415f-995c-b4091868bc81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c4e29bb-1ca6-4b92-975d-caee79c3231d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46be850-1263-415f-995c-b4091868bc81",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "e02973c5-fc94-41ee-ae46-f9f57a54f7ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "e7d13f63-e4a3-47af-8459-238873e6ae93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e02973c5-fc94-41ee-ae46-f9f57a54f7ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad1af774-2590-44ce-bceb-7ee5dc5adb5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e02973c5-fc94-41ee-ae46-f9f57a54f7ec",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "4fce490e-a7a7-448e-9e24-9e9bd97223ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "a01e2465-158e-4918-bc10-d7e323cff9d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fce490e-a7a7-448e-9e24-9e9bd97223ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949a5d96-b62a-4452-bdce-9c3ef8db7795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fce490e-a7a7-448e-9e24-9e9bd97223ed",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "e74e7b1f-91c7-4e30-aabe-88383aaacfae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "30ae11a4-567b-4c5a-8d86-9b9a51b58ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74e7b1f-91c7-4e30-aabe-88383aaacfae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0cf565-8716-4ce6-a04a-da8dab1152e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74e7b1f-91c7-4e30-aabe-88383aaacfae",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "4e393a19-28a5-460e-91a1-85b6d9154556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "beaae8ad-51c9-4dd4-95ea-44f8e3ca31cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e393a19-28a5-460e-91a1-85b6d9154556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "934566aa-9c2f-4f10-8827-0ec39051e6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e393a19-28a5-460e-91a1-85b6d9154556",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        },
        {
            "id": "b87fda4a-e43d-4ab4-a962-2370e3db7b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "compositeImage": {
                "id": "ae0c753d-7f01-403a-a655-28f304076d3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b87fda4a-e43d-4ab4-a962-2370e3db7b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee2b071-1298-4ff1-b89e-3109c23b59a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b87fda4a-e43d-4ab4-a962-2370e3db7b72",
                    "LayerId": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3f654e1c-ea5e-4a15-a4eb-fdaae2067dbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbc483c3-97d9-485c-aa88-6cc29ccb2c3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}